FROM keymetrics/pm2:latest-alpine
LABEL MAINTAINER="instane@gmail.com"
LABEL version="1.1.1"

ENV API_PORT 3000
ENV API_HOST 0.0.0.0
ENV SOCKET_PORT 3001
ENV SOCKET_HOST 0.0.0.0

RUN mkdir -p /app
WORKDIR /app
COPY ./ /app

RUN yarn install --non-interactive --silent && yarn build

CMD [ "pm2-runtime", "start", "ecosystem.config.js", "--env", "production" ]
EXPOSE 3000 3001

