import { Room } from "../entity/Room";
import { Service } from "moleculer";
import * as Koa from "koa";
import * as Router from "koa-router";
import * as bodyParser from "koa-bodyparser";
import * as cors from "@koa/cors";
import { broker } from "../config/broker";
import { Video } from "../entity/Video";
import { VideoThumbnail, ThumbnailTypes } from "../entity/VideoThumbnail";
import { User } from "../entity/User";
import * as passport from 'koa-passport';
import { Strategy as GoogleStrategy } from 'passport-google-auth'
import * as session from 'koa-session'

export const configureHTTPService = async () => {
  class HTTPService extends Service {
    private app: Koa;
    private router: Router;

    constructor(broker) {
      super(broker);

      this.parseServiceSchema({
        name: "http",
        version: 1,
        settings: {
          $noVersionPrefix: true
        },
        meta: {
          scalable: false
        },

        dependencies: [
          { name: "user", version: 1 },
          { name: "room", version: 1 },
          { name: "video", version: 1 },
          { name: "youtube", version: 1 }
        ],
        created: this.created,
        started: this.started,
        stopped: this.stopped
      });
    }

    async getAllRooms(ctx) {
      ctx.body = await broker.call("room.getAll");
    }

    async getOneRoom(ctx) {
      const { id } = ctx.params;
      const room = await broker.call("room.get", { id });
      ctx.body = room ? room : {};
    }

    async getOneRoomVideos(ctx) {
      broker.logger.info('getOneRoomVideos')
      const { id } = ctx.params;
      console.log(id);
      const videos = await broker.call("video.getAllFromRoom", { roomId: id });
      console.log(videos);
      ctx.body = videos ? videos : [];
    }

    async createRoom(ctx) {
      let name: string;
      name = ctx.request.body.name;
      if (!name || !name.length) {
        ctx.body = {};
        return;
      }
      let room = new Room();
      room.name = name;
      ctx.body = await broker.call("room.create", { room });
    }

    async getAllUsers(ctx) {
      ctx.body = await broker.call("user.getAll");
    }

    async getAllVideos(ctx) {
      ctx.body = await broker.call("video.getAll");
    }

    async getOneVideo(ctx) {
      const { id } = ctx.params;
      const video = await broker.call("video.get", { id });
      ctx.body = video ? video : {};
    }

    async createVideo(ctx) {
      let url: string, roomId: number;
      url = ctx.request.body.url;
      roomId = ctx.request.body.roomId;
      if (!url || !url.length || !roomId || roomId < 1) {
        ctx.body = { status: 'error', message: `Url "${url}" is invalid` };
        return;
      }
      const youtubeVideo = await broker.call("youtube.getVideoByUrl", { url });

      let room = await broker.call("room.get", { id: roomId })
      let video = new Video();
      video.channelId = youtubeVideo.channelId;
      // video.description = youtubeVideo.description;
      video.description = '';
      // console.log(youtubeVideo.description)
      video.minutes = youtubeVideo.minutes;
      video.seconds = youtubeVideo.seconds;
      video.title = youtubeVideo.title;
      video.url = youtubeVideo.url;
      video.vid = youtubeVideo.id;
      video.room = room;

      video.thumbnails = Object.keys(youtubeVideo.thumbnails).map(type => {
        const youtubeThumbnail = youtubeVideo.thumbnails[type];
        let thumbnail = new VideoThumbnail();
        thumbnail.type = ThumbnailTypes[type];
        thumbnail.height = youtubeThumbnail.height;
        thumbnail.width = youtubeThumbnail.width;
        thumbnail.url = youtubeThumbnail.url;
        return thumbnail;
      });

      if (!room.currentVideo) {
        video.playing = true
        // video = await broker.call("video.create", { video })
      } else {
        video.playing = false
      }

      video = await broker.call("video.create", { video })
      // broker.logger.info(video)

      if (!room.currentVideo) {
        // video.playing = true
        // video = await broker.call("video.create", { video })
        room.currentVideo = video
        await broker.call("room.create", { room })
      }
      
      // ctx.body = video
      const { id, vid, minutes, seconds, title } = video
      ctx.body = { id, vid, minutes, seconds, title }
    }

    async logout(ctx) {
      ctx.logout()
      ctx.redirect('/me')
    }

    async getMe(ctx) {
      ctx.body = { user: ctx.state.user, isAuthenticated: ctx.isAuthenticated() }
    }

    validateId(id, ctx, next) {
      if (!id) {
        ctx.body = {};
        return;
      }
      id = +id;
      if (isNaN(id) || id < 1) {
        ctx.body = {};
        return;
      }
      ctx.params.id = id;
      return next();
    }

    created() {
      this.app = new Koa();
      this.router = new Router();
      this.app.keys = [process.env.APP_SECRET]

      passport.serializeUser((user, done) => {
        console.log('useer!', user, user.id)
        done(null, `${user.id}`)
      })

      passport.deserializeUser(async (id, done) => {
        try {
          console.log('id', id)
          let user = await broker.call("user.get", { id: +id });
          if (!user) {
            done('user not found')
          } else {
            done(null, user)
          }
        } catch (err) {
          done(err)
        }
      })

      passport.use(new GoogleStrategy({
        clientId: process.env.GOOGLE_OAUTH_ID,
        clientSecret: process.env.GOOGLE_OAUTH_SECRET,
        callbackURL: process.env.GOOGLE_OAUTH_CALLBACK
      },
        async (token, tokenSecret, profile, done) => {
          // retrieve user ...
          console.log('profile,', profile)
          const users = await broker.call("user.getByGoogleId", { googleId: profile.id })
          let user: User
          if (users.length !== 1) {
            user = new User;
            user.googleId = profile.id
            user.name = profile.nickname
            user.email = profile.emails[0].value
            user = await broker.call("user.create", { user })
          } else {
            user = users[0]
          }
          // done(null, { id: profile.id, nickname: profile.nickname, displayName: profile.displayName, email: profile.emails[0].value })
          done(null, user)
        }
      ))

      this.router
        .param("id", this.validateId)
        .get("/rooms", this.getAllRooms)
        .get("/rooms/:id", this.getOneRoom)
        .get("/rooms/:id/videos", this.getOneRoomVideos)
        .post("/rooms", this.createRoom)
        .get("/videos", this.getAllVideos)
        .get("/videos/:id", this.getOneVideo)
        .post("/videos", this.createVideo)
        .get("/users", this.getAllUsers)
        .get("/auth/google", passport.authenticate('google'))
        .get("/auth/google/callback", passport.authenticate('google', {
          successRedirect: '/me',
          failureRedirect: '/'
        }))
        .get("/logout", this.logout)
        .get("/me", this.getMe)

      this.app
        .use(bodyParser())
        .use(cors({ credentials: true }))
        .use(session({
          key: 'ctsession',
          domain: process.env.SESSION_DOMAIN
        }, this.app))
        .use(passport.initialize())
        .use(passport.session())
        .use(this.router.routes())
        .use(this.router.allowedMethods())
      // .use(bodyParser())


      this.logger.info("ES6 Service created.");
    }

    async started() {
      this.app.listen(process.env.API_PORT, process.env.API_HOST);
      // function listeningReporter() {
      //   // `this` refers to the http server here
      //   const { address, port } = this.address();
      //   const protocol = this.addContext ? 'https' : 'http';
      //   console.log(`Listening on ${protocol}://${address}:${port}...`);
      // }

      this.logger.info("ES6 Service started.");
    }

    async stopped() {
      this.logger.info("ES6 Service stopped.");
    }
  }

  return broker.createService(HTTPService);
};
