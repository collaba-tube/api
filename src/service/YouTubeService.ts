import { Service } from "moleculer";
import { broker } from "../config/broker";
import { YouTube } from "better-youtube-api";

export const configureYouTubeService = async () => {
  class YouTubeService extends Service {
    private youtube: YouTube;

    constructor(broker) {
      super(broker);

      this.parseServiceSchema({
        name: "youtube",
        version: 1,
        settings: {
          $noVersionPrefix: true
        },
        meta: {
          scalable: true
        },
        actions: {
          getVideo: {
            handler: this.getVideo,
            params: {
              id: "string"
            }
          },
          getVideoByUrl: {
            handler: this.getVideoByUrl,
            params: {
              url: "string"
            }
          }
        },
        created: this.created,
        started: this.started,
        stopped: this.stopped
      });
    }

    created() {
      this.youtube = new YouTube(process.env.YOUTUBE_API_KEY);
      this.logger.info("ES6 Service created.");
    }

    async started() {
      this.logger.info("ES6 Service started.");
    }

    async stopped() {
      this.logger.info("ES6 Service stopped.");
    }

    async getVideo(ctx) {
      // dQw4w9WgXcQ
      const { id } = ctx.params;
      this.logger.info("getVideo called.");
      return await this.youtube.getVideo(id);
    }

    async getVideoByUrl(ctx) {
      // https://youtube.com/watch?v=dQw4w9WgXcQ
      const { url } = ctx.params;
      this.logger.info("getVideoByUrl called.");
      return await this.youtube.getVideoByUrl(url);
    }
  }
  return broker.createService(YouTubeService);
};
