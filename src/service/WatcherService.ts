import { Service } from "moleculer";
import { broker } from "../config/broker";

export const configureWatcherService = async () => {
  class WatcherService extends Service {
    constructor(broker) {
      super(broker);

      this.parseServiceSchema({
        name: "watcher",
        version: 1,
        settings: {
          $noVersionPrefix: true
        },
        meta: {
          scalable: false
        },

        dependencies: [{ name: "room", version: 1 }],

        created: this.created,
        started: this.started,
        stopped: this.stopped
      });
    }

    created() {
      this.logger.info("ES6 Service created.");
    }

    async started() {
      this.logger.info("ES6 Service started.");

      setInterval(() => {
        broker.call("video.watchPlaylistUpdate");
      }, 1000);
    }

    async stopped() {
      this.logger.info("ES6 Service stopped.");
    }
  }

  return broker.createService(WatcherService);
};
