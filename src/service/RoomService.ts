import { Service } from "moleculer";
import { Room } from "../entity/Room";
import { broker } from "../config/broker";
import { createConnectionWithTimeout, connection } from "../config/connection";
import { Repository, Connection } from "typeorm";

export const configureRoomService = async () => {
  class RoomService extends Service {
    private connection: Connection;
    private repository: Repository<Room>;

    constructor(broker) {
      super(broker);

      this.parseServiceSchema({
        name: "room",
        version: 1,
        settings: {
          $noVersionPrefix: true
        },
        meta: {
          scalable: true
        },
        actions: {
          getAll: this.getAll,
          get: {
            handler: this.get,
            params: {
              id: "number"
            }
          },
          watchPlaylistUpdate: this.watchPlaylistUpdate,
          create: {
            handler: this.create,
            params: {
              room: "object"
            }
          },
          save: {
            handler: this.save,
            params: {
              room: "object"
            }
          }
        },
        // events: {
        //     "user.created": this.userCreated
        // },
        created: this.created,
        started: this.started,
        stopped: this.stopped
      });
    }

    created() {
      this.logger.info("ES6 Service created.");
    }

    async started() {
      this.logger.info("ES6 Service started.");

      // this.connection = await createConnectionWithTimeout();
      this.connection = await connection;
      this.repository = this.connection.getRepository(Room);
    }

    async stopped() {
      this.logger.info("ES6 Service stopped.");
    }

    async getAll(ctx) {
      this.logger.info("getAll called.");

      return await this.repository.find();
    }

    async get(ctx) {
      this.logger.info("get called.");

      const { id } = ctx.params;
      return await this.repository.findOne(id);
    }

    async watchPlaylistUpdate(ctx) {
      // this.logger.info('watchPlaylistUpdate');
    }

    async create(ctx) {
      const { room } = ctx.params;
      return await broker.call('room.save', { room })
      // this.logger.info(room)
      // await this.repository.save(room);
      // return room;
    }

    async save(ctx) {
      const { room } = ctx.params;
      await this.repository.save(room);
      return room;
    }
  }
  return broker.createService(RoomService);
};
