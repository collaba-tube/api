import { Service } from "moleculer";
import { broker } from "../config/broker";
import { createConnectionWithTimeout, connection } from "../config/connection";
import { Repository, Connection } from "typeorm";
import { User } from "../entity/User";

export const configureUserService = async () => {
  class UserService extends Service {
    private connection: Connection;
    private repository: Repository<User>;

    constructor(broker) {
      super(broker);

      this.parseServiceSchema({
        name: "user",
        version: 1,
        settings: {
          $noVersionPrefix: true
        },
        meta: {
          scalable: true
        },
        actions: {
          getAll: this.getAll,
          get: {
            handler: this.get,
            params: {
              id: "number"
            }
          },
          getByGoogleId: {
            handler: this.getByGoogleId,
            params: {
              googleId: "string"
            }
          },
          create: {
            handler: this.create,
            params: {
              user: "object"
            }
          }
        },
        created: this.created,
        started: this.started,
        stopped: this.stopped
      });
    }

    created() {
      this.logger.info("ES6 Service created.");
    }

    async started() {
      this.logger.info("ES6 Service started.");

      // this.connection = await createConnectionWithTimeout();
      this.connection = await connection;
      this.repository = this.connection.getRepository(User);
    }

    async stopped() {
      this.logger.info("ES6 Service stopped.");
    }

    async getAll(ctx) {
      this.logger.info("getAll called.");

      return await this.repository.find();
    }

    async get(ctx) {
      this.logger.info("get called.");

      const { id } = ctx.params;
      return await this.repository.findOne(id);
    }

    async getByGoogleId(ctx) {
        this.logger.info("getByGoogleId called.");
  
        const { googleId } = ctx.params;
        return await this.repository.find({ googleId });
      }

    async create(ctx) {
      const { user } = ctx.params;
      await this.repository.save(user);
      return user;
    }
  }
  return broker.createService(UserService);
};
