import { Service } from "moleculer";
import { broker } from "../config/broker";
import { createConnectionWithTimeout, connection } from "../config/connection";
import { Repository, Connection } from "typeorm";
import { Video } from "../entity/Video";
import { Room } from "../entity/Room";
import { addMinutes, addSeconds, addYears } from 'date-fns'

export const configureVideoService = async () => {
  class VideoService extends Service {
    private connection: Connection;
    private repository: Repository<Video>;

    constructor(broker) {
      super(broker);

      this.parseServiceSchema({
        name: "video",
        version: 1,
        settings: {
          $noVersionPrefix: true
        },
        meta: {
          scalable: true
        },
        dependencies: [{ name: "room", version: 1 }],
        actions: {
          getAll: this.getAll,
          getAllFromRoom: {
            handler: this.getAllFromRoom,
            params: {
              roomId: "number"
            }
          },
          get: {
            handler: this.get,
            params: {
              id: "number"
            }
          },
          create: {
            handler: this.create,
            params: {
              video: "object"
            }
          },
          save: {
            handler: this.save,
            params: {
              video: "object"
            }
          },
          delete: {
            handler: this.delete,
            params: {
              video: "object"
            }
          },
          watchPlaylistUpdate: this.watchPlaylistUpdate
        },
        // events: {
        //     "user.created": this.userCreated
        // },
        created: this.created,
        started: this.started,
        stopped: this.stopped
      });
    }

    created() {
      this.logger.info("ES6 Service created.");
    }

    async started() {
      this.logger.info("ES6 Service started.");

      // this.connection = await createConnectionWithTimeout();
      this.connection = await connection;
      this.repository = this.connection.getRepository(Video);
    }

    async stopped() {
      this.logger.info("ES6 Service stopped.");
    }

    async getAll(ctx) {
      this.logger.info("getAll called.");

      return await this.repository.find();
    }

    async getAllFromRoom(ctx) {
      const { roomId } = ctx.params;
      this.logger.info("getAllFromRoom called.");

      const videos = await this.repository
        .createQueryBuilder("video")
        .where("roomId = :roomId")
        .setParameters({ roomId })
        .getMany();

      return videos;
    }

    async get(ctx) {
      this.logger.info("get called.");

      const { id } = ctx.params;
      return await this.repository.findOne(id);
    }

    async watchPlaylistUpdate(ctx) {
      let videos = await this.repository
        .createQueryBuilder("video")
        .leftJoinAndSelect("video.room", "room")
        .where("video.endAt < :now")
        .andWhere("video.playing = :playing")
        .setParameters({ now: new Date, playing: true })
        .getMany();

      videos.forEach(async (video) => {
        video.playing = false
        video = await broker.call('video.save', { video })

        const room = video.room;
        let nextVideo = await this.repository
          .createQueryBuilder("video")
          .where("video.beginAt = :beginAt")
          .setParameters({ beginAt: video.endAt })
          .getOne();
        this.logger.info('nextVideo', nextVideo)
        if (nextVideo) {
          nextVideo.playing = true
          nextVideo = await broker.call('video.save', { video: nextVideo })
          room.currentVideo = nextVideo
        } else {
          room.currentVideo = null
        }
        await broker.call('room.save', { room })
      })
    }

    async delete(ctx) {
      const { video } = ctx.params;
      this.repository.delete(video);
    }

    async create(ctx) {
      const { video } = ctx.params;
      const latestVideo = await this.repository.findOne({ order: { endAt: "DESC" } })
      let endAt: Date
      if (!latestVideo || !latestVideo.playing) {
        endAt = new Date
      } else {
        endAt = latestVideo.endAt
      }
      this.logger.info(latestVideo, 'latestVideo')
      video.beginAt = endAt
      video.endAt = addSeconds(addMinutes(video.beginAt, video.minutes), video.seconds)
      // await this.repository.save(video);
      // return video;
      return await broker.call('video.save', { video })
    }

    async save(ctx) {
      const { video } = ctx.params;
      await this.repository.save(video);
      return video;
    }
  }
  return broker.createService(VideoService);
};
