import { Service } from "moleculer";
import { broker } from "../config/broker";
import * as SocketIO from 'moleculer-socketio';
import * as _ from "lodash";

export const configureSocketService = async () => {
    class SocketService extends Service {
        private state: any;

        constructor(broker) {
            super(broker);

            this.parseServiceSchema({
                mixins: [SocketIO],
                name: "socket",
                version: 1,
                settings: {
                    port: process.env.SOCKET_PORT,
                    $noVersionPrefix: true
                },
                meta: {
                    scalable: true
                },
                namespaces: {
                    "/": {
                        connection: this.connection,
                        roomUsersCount: this.roomUsersCount,
                        roomConnection: {
                            params: {
                                roomId: "number"
                            },
                            handler: this.roomConnection
                        },
                        roomDisconnection: {
                            params: {
                                roomId: "number"
                            },
                            handler: this.roomDisconnection
                        },
                        joinChat: {
                            params: {
                                roomId: "number"
                            },
                            handler: this.joinChat
                        }
                    },
                },
                created: this.created,
                started: this.started,
                stopped: this.stopped
            });
        }

        disconnect = () => {
            --this.state.users;
            this.logger.info("Someone is disconnected");
            this.logger.info(this.state.users);
            this.io.emit('userCountChange', { users: this.state.users });
        }

        connection(ctx) {
            ++this.state.users;
            this.logger.info("Someone is connected");
            this.logger.info(this.state.users);
            this.io.emit('userCountChange', { users: this.state.users })
            ctx.socket.on('disconnect', this.disconnect)
        }

        roomConnection(ctx) {
            const { roomId, username } = ctx.params;
            this.logger.info("Someone is connected to room", roomId);
            if (!_.has(this.state.rooms, roomId)) {
                this.state.rooms[roomId] = 0;
            }
            ++this.state.rooms[roomId];
            if (!_.has(this.state.roomsUsers, roomId)) {
                this.state.roomsUsers[roomId] = [];
            }
            this.state.roomsUsers[roomId].push(username)
            this.io.emit("roomUserCountChange", { roomId, users: this.state.rooms[roomId], userNames: this.state.roomsUsers[roomId] });
            ctx.socket.on('disconnect', () => {
                --this.state.rooms[roomId];
                if (this.state.rooms[roomId] < 0) {
                    this.state.rooms[roomId] = 0
                }
                this.state.roomsUsers[roomId] = this.state.roomsUsers[roomId].filter((_username) => {
                    return _username !== username
                })
                this.logger.info("Someone is disconnected from room", roomId);
                this.io.emit("roomUserCountChange", { roomId, users: this.state.rooms[roomId], userNames: this.state.roomsUsers[roomId] })
            })
        }

        roomDisconnection(ctx) {
            this.logger.info("hit roomDisconnection");
            const { roomId } = ctx.params;
            this.logger.info("Someone is disconnected from room", roomId);
            if (!_.has(this.state.rooms, roomId)) {
                this.state.rooms[roomId] = 1;
            }
            --this.state.rooms[roomId];
            this.io.emit("roomUserCountChange", { roomId, users: this.state.rooms[roomId] })
        }

        roomUsersCount(ctx) {
            this.logger.info(this.state.rooms);
            ctx.socket.emit('roomUsersCount', { rooms: this.state.rooms })
        }

        joinChat(ctx) {
            const { roomId } = ctx.params;
            let { username } = ctx.params;
            const generateName = require('sillyname');
            if (username === 'anon') {
                username = generateName();
            }
            const roomName = `room${roomId}`;
            ctx.socket.join(roomName);
            // const leave = ctx.socket.leave;
            // console.log('yyy', ctx.socket.leave, ctx.socket.join, this.io.sockets.leave)
            ctx.socket.on('disconnect', () => {
              this.logger.info('chatLeave', username)
                //ctx.socket.leave(roomName)
                //leave(roomName)
            })
            ctx.socket.on('chatDisconnect', (data) => {
              this.logger.info('chatDisconnect', username, roomName, data)
                ctx.socket.leave(roomName, (data) => {
                  console.log('sfa3tfg434gt',data)
                })
              this.logger.info('leaved')
                //leave(roomName)
            })
            ctx.socket.on('message', ({text}) => {
                this.logger.info(text);
                this.io.sockets.in(roomName).emit('newMessage', {username, text });

            });
        }

        created() {
            this.state = {
                users: 0,
                rooms: {

                },
                roomsUsers: []
            };
            this.logger.info("ES6 Service created.");
        }

        async started() {
            // this.app.listen(3002);

            this.logger.info("ES6 Service started.");
        }

        async stopped() {
            this.logger.info("ES6 Service stopped.");
        }
    }
    return broker.createService(SocketService);
};
