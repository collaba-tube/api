import { ConnectionOptions, createConnection, Connection } from "typeorm";

export const connectionOptions: ConnectionOptions = {
  type: "mariadb",
  host: process.env.DB_HOST,
  port: +process.env.DB_PORT,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  synchronize: true,
  logging: false,
  entities: [`${__dirname}/../entity/*.{ts,js}`],
  migrations: [`${__dirname}/../migration/*.{ts,js}`],
  subscribers: [`${__dirname}/../subscriber/*.{ts,js}`],
  cli: {
    entitiesDir: `${__dirname}/../entity`,
    migrationsDir: `${__dirname}/../migration`,
    subscribersDir: `${__dirname}/../subscriber`
  }
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export const createConnectionWithTimeout = async (
  timeout: number = 3000,
  retryCounts: number = 5
): Promise<Connection> => {
  let connection: Connection;
  let lastError;
  console.log("lets try to create connection");
  for (let i = 1; i <= retryCounts; ++i) {
    console.log("attempt: ", i);
    try {
      connection = await createConnection(connectionOptions);
      console.log("success");
      return connection;
    } catch (e) {
      console.log("failure", e);
      lastError = e;
    }
    await sleep(timeout);
  }
  throw lastError;
};

export const connection = createConnection(connectionOptions);