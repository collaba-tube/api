import { ServiceBroker } from "moleculer";

export const broker = new ServiceBroker({
  transporter: process.env.NATS_URL,
  logLevel: process.env.APP_ENV && process.env.APP_ENV === 'dev' ? "info" : "error",
  requestTimeout: 5 * 1000
});

broker.start();
