import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from "typeorm";
import { Room } from "./Room";
import { VideoThumbnail } from "./VideoThumbnail";

@Entity()
export class Video {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  vid: string;

  @Column()
  url: string;

  @Column()
  channelId: string;

  @Column("text")
  description: string;

  @Column()
  minutes: number;

  @Column()
  seconds: number;

  @Column()
  title: string;

  @ManyToOne(type => Room, room => room.playlist)
  room: Room;

  @OneToMany(type => VideoThumbnail, thumbnail => thumbnail.video, {eager: true})
  thumbnails: VideoThumbnail[];

  @Column()
  beginAt: Date

  @Column()
  endAt: Date

  @Column()
  playing: boolean
}
