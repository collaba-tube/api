import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn } from "typeorm";
import { Video } from "./Video";

@Entity()
export class Room {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(type => Video, video => video.room)
  playlist: Video[];

  // @Column()
  // currentVideoTitle: string;

  @OneToOne(type => Video, {eager: true})
  @JoinColumn()
  currentVideo: Video
}
