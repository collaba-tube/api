import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Video } from "./Video";

export enum ThumbnailTypes {
  default,
  high,
  maxres,
  medium,
  standard
}

@Entity()
export class VideoThumbnail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  type: ThumbnailTypes;

  @Column()
  height: number;

  @Column()
  width: number;

  @Column()
  url: string;

  @ManyToOne(type => Video, video => video.thumbnails)
  video: Video;
}
