require("dotenv").config();
import "reflect-metadata";
import { configureHTTPService } from "./service/HTTPService";
import { configureRoomService } from "./service/RoomService";
import { configureVideoService } from "./service/VideoService";
import { configureYouTubeService } from "./service/YouTubeService";
import { configureWatcherService } from "./service/WatcherService";
import { configureSocketService } from "./service/SocketService";
import { configureUserService } from "./service/UserService";

(async function start() {
  await configureRoomService();
  await configureVideoService();
  await configureYouTubeService();
  await configureHTTPService();
  await configureWatcherService();
  await configureSocketService();
  await configureUserService();
})().catch(e => {
  console.log(e);
  process.exit(1);
});
